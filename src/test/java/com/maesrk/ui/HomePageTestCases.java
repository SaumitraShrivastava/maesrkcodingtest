package com.maesrk.ui;

import com.maesrk.pages.*;
import com.maesrk.util.Generic;
import com.maesrk.util.WebdriverModule;
import com.maesrk.config.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class HomePageTestCases extends BaseClass {
    private WebDriver webDriver;
    private HomePage homePage;
    private WebdriverModule webdriverModule;

    @BeforeClass
    public void beforeClass() {
        System.setProperty("webdriver.chrome.driver", super.chromeDriverPath);
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webdriverModule = new WebdriverModule(webDriver);
        homePage = new HomePage(webDriver);
    }


    @Test
    public void TestCase1() throws Exception {
        webdriverModule.launchURL(super.instanceURL);
        long start, end;
        String setTimer = "69";
        homePage.setTimer(setTimer);
        String timer = homePage.getProgressText();
        int seconds = Generic.convertTimerInSecs(timer);
        for (int i = seconds; i > 0; i--) {
            start = System.currentTimeMillis();
            Assert.assertEquals(homePage.getProgressText(), Generic.getTimer(i));
            end = System.currentTimeMillis();
            Thread.sleep(1000 - (end - start));
        }
        Thread.sleep(1000);
        Assert.assertEquals(webDriver.switchTo().alert().getText(), "Time Expired!");
    }

    @AfterClass
    public void afterClass() {
        webDriver.quit();
    }
}
