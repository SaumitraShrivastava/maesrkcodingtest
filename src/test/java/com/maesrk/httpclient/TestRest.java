package com.maesrk.httpclient;

import com.maesrk.config.BaseClass;
import com.maesrk.httprestclient.RestClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestRest extends BaseClass {
    private RestClient restClient;
    private CloseableHttpResponse closeableHttpResponse;


    @Test(description = "Status code should be 200")
    public void testGet1() throws IOException {
        restClient = new RestClient(getURL);
        closeableHttpResponse = restClient.httpGetCallWithoutHeader();
        String jsonResponseEntity = EntityUtils.toString(closeableHttpResponse.getEntity());
        Assert.assertEquals(closeableHttpResponse.getStatusLine().getStatusCode(), 200);
        System.out.println(jsonResponseEntity);
    }

    @Test(description = "Validate ID and other attributes")
    public void testGet2() throws IOException {
        restClient = new RestClient(getURL);
        closeableHttpResponse = restClient.httpGetCallWithoutHeader();
        String jsonResponseEntity = EntityUtils.toString(closeableHttpResponse.getEntity());
        Assert.assertEquals(closeableHttpResponse.getStatusLine().getStatusCode(), 200);
        Assert.assertEquals(restClient.getValueFromJsonPath("tbd", jsonResponseEntity), "false");
        Assert.assertEquals(restClient.getValueFromJsonPath("net", jsonResponseEntity), "false");
        Assert.assertEquals(restClient.getValueFromJsonPath("rocket", jsonResponseEntity), "5e9d0d95eda69973a809d1ec");
        Assert.assertEquals(restClient.getValueFromJsonPath("success", jsonResponseEntity), "true");
        Assert.assertEquals(restClient.getValueFromJsonPath("launchpad", jsonResponseEntity), "5e9e4502f509094188566f88");
        Assert.assertEquals(restClient.getValueFromJsonPath("auto_update", jsonResponseEntity), "true");
        Assert.assertEquals(restClient.getValueFromJsonPath("flight_number", jsonResponseEntity), "102");
        Assert.assertEquals(restClient.getValueFromJsonPath("id", jsonResponseEntity), "5ef6a1e90059c33cee4a828a");
    }
}
