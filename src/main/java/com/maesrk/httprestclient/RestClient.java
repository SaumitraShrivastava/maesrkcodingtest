package com.maesrk.httprestclient;

import io.restassured.path.json.JsonPath;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RestClient {
    private CloseableHttpClient closeableHttpClient;
    private CloseableHttpResponse closeableHttpResponse;
    private String url;

    public RestClient(String url) {
        this.url = url;
    }

    public CloseableHttpResponse httpGetCallWithoutHeader() throws IOException {
        closeableHttpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        this.closeableHttpResponse = closeableHttpClient.execute(httpGet);
        return this.closeableHttpResponse;
    }

    public String getValueFromJsonPath(String jsonPath, String jsonString) throws IOException {
        return new JsonPath(jsonString).get(jsonPath).toString();
    }

}
