package com.maesrk.util;

import java.util.Random;

public class Generic {
    public static int generateRandomDigit() {
        Random random = new Random();
        int x = random.nextInt(900) + 100;
        return x;
    }

    public static String getTimer(int timerInSeconds) throws Exception {
        int min, sec;
        String timerText;
        min = timerInSeconds / 60;
        sec = timerInSeconds % 60;
        String minPlural = min > 1 ? "s" : "";
        String secPlural = sec > 1 ? "s" : "";
        if (min == 0) {
            timerText = sec + " second" + secPlural;
        } else if (sec == 0) {
            timerText = min + " minute" + minPlural;
        } else {
            timerText = min + " minute" + minPlural + " " + sec + " second" + secPlural;
        }
        return timerText;
    }

    public static int convertTimerInSecs(String timer) {
        String[] splitStr = timer.split(" ");
        int min = timer.toLowerCase().contains("minute") ? Integer.parseInt(splitStr[0]) : 0;
        int sec = timer.toLowerCase().contains("seconds") ? Integer.parseInt(splitStr[splitStr.length - 2]) : 0;
        return min * 60 + (sec);
    }

    public static void main(String[] args) throws Exception {
        for (int i = 62; i > 0; i--) {
            System.out.println(getTimer(i));
            Thread.sleep(1000);
        }
    }
}
