package com.maesrk.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebdriverModule {
   private WebDriver webDriver;

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public WebdriverModule(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void launchURL(String url){
        webDriver.get(url);
    }

    public void quirWebdriver(){
        webDriver.quit();
    }

    public void movetoElement(WebElement element){
        Actions action =new Actions(webDriver);
        action.moveToElement(element).build().perform();
    }

    public void waitForElementToVisible(By element, long timeinSec){
        WebDriverWait webDriverWait= new WebDriverWait(webDriver, timeinSec);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
}
