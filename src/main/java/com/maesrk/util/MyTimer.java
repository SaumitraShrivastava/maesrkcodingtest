package com.maesrk.util;

import java.util.TimerTask;

public class MyTimer {


    public static void main(String[] args) throws Exception {
        int timet = 120;
        long delay = timet * 1000;
        do {
            int minutes = timet / 60;
            int seconds = timet % 60;
            System.out.println(minutes + " minute(s), " + seconds + " second(s)");
            Thread.sleep(1000);
            timet = timet - 1;
            delay = delay - 1000;

        }
        while (delay != 0);
        System.out.println("Time's Up!");
    }
}
