package com.maesrk.pages;

import com.maesrk.util.WebdriverModule;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends WebdriverModule {

    @FindBy(id = "start_a_timer")
    private WebElement timerTextField;

    @FindBy(id = "timergo")
    private WebElement timerGoBtn;

    @FindBy(id = "progressText")
    private WebElement progressText;


    public HomePage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void setTimer(String timer) {
        timerTextField.clear();
        timerTextField.sendKeys(timer);
        timerGoBtn.click();
    }

    public String getProgressText() {
        return progressText.getText();
    }
}
